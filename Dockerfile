# Use the official Python image as the base image
FROM python:3.8

# Set environment variables
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=ToDo.settings
ENV ALLOWED_HOSTS='0.0.0.0,localhost'

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app/

# Install gunicorn
RUN pip3 install gunicorn

# Install dependencies
RUN pip3 install --no-cache-dir -r requirements.txt

# Modify the ALLOWED_HOSTS directly in settings.py
RUN sed -i 's/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS = ["0.0.0.0"]/g' ToDo/settings.py

# Collect static files
RUN python3 manage.py collectstatic --noinput

# Expose the port the application runs on
EXPOSE 8000

# Run the Django application with Gunicorn
CMD ["gunicorn", "ToDo.wsgi:application", "--bind", ":8000"]