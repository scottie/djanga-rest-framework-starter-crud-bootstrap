from django.contrib import admin
from .models import Item, Task

admin.site.register(Item)
admin.site.register(Task)
