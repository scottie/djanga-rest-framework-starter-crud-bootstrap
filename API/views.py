# from rest_framework.decorators import api_view
# from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework import generics  # Using class based views
from rest_framework.views import APIView
from .models import Item, Task
from .serializer import ItemSerializer, TaskSerializer
from datetime import datetime
import time
import os

# using function based view


def get_uptime():
    """
    Returns the current uptime.
    """
    # Get the timestamp when the application was started
    started_at = datetime.fromtimestamp(os.path.getctime(__file__))

    # Calculate the current uptime
    current_time = datetime.now()
    uptime = current_time - started_at

    return uptime

# @api_view(['GET'])
#  def apiOverview(request):
#    res = {
#        "success": True
#    }

#    return Response(res)

# Using generic class based views


class StatusView(APIView):
    def get(self, request, format=None):
        data = {
            "Status": True,
            "DateTime": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            "TimeStamp": int(time.time()),
            "UpTime": get_uptime()
        }
        return Response(data)


class ItemList(generics.CreateAPIView):
    serializer_class = ItemSerializer
    # permission_classes = [IsAdminUser]

    def get_queryset(self):
        queryset = Item.objects.all()
        task = self.request.query_params.get('task')
        if task is not None:
            queryset = queryset.filter(itemMainTask=task)
        return queryset


class ItemList(generics.ListCreateAPIView):
    serializer_class = ItemSerializer

    def get_queryset(self):
        queryset = Item.objects.all()
        task = self.request.query_params.get('task')
        if task is not None:
            queryset = queryset.filter(itemMainTask=task)
        return queryset


class ItemDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()


class TaskList(generics.ListCreateAPIView):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()


class TaskDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
