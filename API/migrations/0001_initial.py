# Generated by Django 4.2.5 on 2023-09-19 12:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('taskName', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('itemName', models.CharField(max_length=100)),
                ('date_added', models.DateField(auto_now_add=True)),
                ('itemStatus', models.BooleanField(default=False)),
                ('itemMainTask', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='API.task')),
            ],
        ),
    ]
