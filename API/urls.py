from django.urls import path
from .views import ItemList, ItemDetail, TaskList, TaskDetail, StatusView

urlpatterns = [
    path('', StatusView.as_view(), name="Status"),
    path('item/', ItemList.as_view()),
    path('item/<int:pk>/', ItemDetail.as_view()),
    path('task/', TaskList.as_view()),
    path('task/<int:pk>/', TaskDetail.as_view()),
]
