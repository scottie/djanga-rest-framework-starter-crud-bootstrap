from django.db import models


class Task(models.Model):
    taskName = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.taskName


class Item(models.Model):
    itemName = models.CharField(max_length=100)
    date_added = models.DateField(auto_now_add=True)
    itemMainTask = models.ForeignKey(Task, on_delete=models.CASCADE)
    itemStatus = models.BooleanField(default=False)

    def __str__(self):
        return self.itemName
