First install django and DRF:

```bash
pip3 install django djangorestframework
```

**Step 1:** Make a project and an app.

```bash
django-admin startproject projectname .
django-admin startapp apiname
```

**Step 2:** Add `INSTALLED_APPS` in the project's settings.py file.

```bash
- Navigate to project settings.py and scroll down to INSTALLED_APPS
- Add "api name".apps.ApiConfig and rest_framework to the INSTALLED_APPS.
```

Also to settings.py add the below for later docker generation:

```
import os
# Set the STATIC_ROOT to a valid filesystem path
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
```

**Step 3:** Create models in the API app (`apiname/models.py`):

```python
from django.db import models

class Location(models.Model):
    locationName = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.locationName

class Item(models.Model):
    itemName = models.CharField(max_length=100)
    date_added = models.DateField(auto_now_add=True)
    itemLocation = models.ForeignKey(Location, on_delete=models.CASCADE)

    def __str__(self):
        return self.itemName
```

**Step 4:** Register models in the admin.py file of the API app (`apiname/admin.py`):

```python
from django.contrib import admin
from .models import Item, Location

admin.site.register(Item)
admin.site.register(Location)
```

**Step 5:** Perform migrations, create a superuser, and run the server.

```bash
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py createsuperuser
python3 manage.py runserver
```

**Step 6:** Create URL patterns and serializer files.

```bash
touch api/urls.py #create api/Urls.py
touch api/serializer.py #Create serializer djangorestframework uses this to convert the data to json
```

- In the project's `urls.py`, add URL patterns:

```python
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('apiname.urls')),  # Replace 'apiname' with your app's name
]
```

**Step 6 (Continued):** Edit the app's `urls.py` file (`apiname/urls.py`):

```python
from django.urls import path
#from .views import ItemList, ItemDetail, LocationList, LocationDetail

urlpatterns = [
    #path('item/', ItemList.as_view()),
    #path('item/<int:pk>/', ItemDetail.as_view()),
    #path('location/', LocationList.as_view()),
    #path('location/<int:pk>/', LocationDetail.as_view()),
]
```

Uncomment the above # once confirmed working and views are made.

**Step 7 (Continued):** Create or edit the serializers file (`apiname/serializers.py`):

```python
from rest_framework import serializers
from .models import Item, Location

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'
```

**Step 8:** Create views in the app's `views.py` file (`apiname/views.py`):

```python
# from rest_framework.decorators import api_view
# from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework import generics  # Using class based views
from rest_framework.views import APIView
from .models import Item, Task
from .serializer import ItemSerializer, TaskSerializer
from datetime import datetime
import time
import os

# using function based view


def get_uptime():
    """
    Returns the current uptime.
    """
    # Get the timestamp when the application was started
    started_at = datetime.fromtimestamp(os.path.getctime(__file__))

    # Calculate the current uptime
    current_time = datetime.now()
    uptime = current_time - started_at

    return uptime

# @api_view(['GET'])
#  def apiOverview(request):
#    res = {
#        "success": True
#    }

#    return Response(res)

# Using generic class based views


class StatusView(APIView):
    def get(self, request, format=None):
        data = {
            "Status": True,
            "DateTime": datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            "TimeStamp": int(time.time()),
            "UpTime": get_uptime()
        }
        return Response(data)


class ItemList(generics.ListCreateAPIView):
    serializer_class = ItemSerializer

    def get_queryset(self):
        queryset = Item.objects.all()
        location = self.request.query_params.get('location')
        if location is not None:
            queryset = queryset.filter(itemLocation=location)
        return queryset

class ItemDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()

class LocationList(generics.ListCreateAPIView):
    serializer_class = LocationSerializer
    queryset = Location.objects.all()

class LocationDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LocationSerializer
    queryset = Location.objects.all()
```

If the code editor can not see "rest_framework" in VSCODE press CTR + SHIFT + P and then type python:select interperator and choose your virtual enviroment.

Now, you have a complete setup that includes serializers, URL patterns, and views for your Django REST framework API. Don't forget to run the server to test your API endpoints:

```bash
python3 manage.py runserver
```

You can access your API at URLs like `/api/item/`, `/api/item/<int:pk>/`, `/api/location/`, and `/api/location/<int:pk>/`.

Now, you should be able to access your API endpoints, such as `/api/item/`, `/api/location/`, and perform CRUD operations.

```
-Check /api/item
-Add locations in /api/location/
-Check /api/location/ID(1)
-Add /api/item
-Check /api/item/?location=1
```

## Install Docker (Ubuntu)

```console
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

```

Add the repositiories apt source:

```bash
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

Install the packages:

```console
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Run the `hello world` image to verify docker is installed:

```console
sudo docker run hello-world
```

Or you can use the script:

```console
curl -fsSL https://get.docker.com -o get-docker.sh
#sudo sh ./get-docker.sh --dry-run #See what it will do
sudo sh ./get-docker.sh
```

IF you do not want to use `sudo` to run docker you can add your user to a group called `docker` but keep in mind any user in that group wil have root privileges:

1.  Create `docker` group

    ```console
    $ sudo groupadd docker

    ```

2.  Add your user to `docker` group

    ```console
    $ sudo usermod -aG docker $USER

    ```

3.  Log out and log back in
        > If in a virtual machine, restart the virtual machine

        Can also run the following command to activate the changes to groups:

        ```console
        $ newgrp docker
        ```
    Run the `hello world` image without `sudo`:

```console
docker run hello-world
```

## Configure Docker to start on boot

For Debian / Ubuntu this is done by default for other distibutions:

```console
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```

## Create Docker file

Using the basic [Django Rest Framework](https://www.django-rest-framework.org/) **CRUD REST API** as a code base we can make a docker image, boilerplate code for the API can be found [here](https://gitlab.com/scottie/djanga-rest-framework-starter-crud-bootstrap)

In the root folder of the project create a file called `Dockerfile`

```docker
# Use the official Python image as the base image
FROM python:3.8

# Set environment variables
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=ToDo.settings
ENV ALLOWED_HOSTS='0.0.0.0,localhost'

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app/

# Install dependencies
RUN pip3 install --no-cache-dir -r requirements.txt

# Install gunicorn
RUN pip3 install gunicorn

# Collect static files
RUN python3 manage.py collectstatic --noinput

# Expose the port the application runs on
EXPOSE 8000
bash
# Run the Django application
CMD ["gunicorn", "ToDo.wsgi:application", "--bind", "0.0.0.0:8000"]
```

## Build Docker Image

To build the Docker image use the below command:

```command
docker build -t todo_django_api .
```

## Run Docker Image

To run the container use the below command:

```command
docker run -p 8000:8000 todo_django_api`
```

## Useful commands

logs

```command
docker logs
docker service logs
#docker todo_django_api logs
```

# GIT LAB STUFF !!

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/scottie/djanga-rest-framework-starter-crud-bootstrap/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name

Choose a self-explaining name for your project.

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

```

```
